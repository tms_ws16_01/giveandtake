##Was ist Give and Take?##
* "Give and Take" dient dazu, Standorte von Einrichtungen anzuzeigen, die gebrauchsfaehige Gueter entgegennehmen und weiter vermitteln.

![alt text](https://bytebucket.org/tms_ws16_01/giveandtake/raw/6e0a9c59990342ab58a868ad52000f2753e6c5eb/Screenshots/ForRM.png?token=270f89393b3b99792b567ddac420bde33931851f)

##Wer braucht die App?##
* Insbesondere Umweltschuetzer und Wohltaeter werden ihren Nutzen darin finden, dass, anstatt ihre Gebrauchgegenstaende wegzuwerfen, sie diese an andere Menschen verschenken koennen. 
* Beduerftige nutzen die App, um Standorte der Einrichtungen zu finden, an denen sie Gebrauchsgegenstaende bekommen koennen.

##Welche Moeglichkeiten bietet mir die Software?##
* Der Nutzer kriegt Informationen ueber die Addresse, die Postleitzahl und die Beschreibung der Einrichtung. Auch die Oeffnungszeiten und die Telefonnummer werden angezeigt. 
* Der Nutzer kann erfahren, ob die Einrichtung kostenlose Abholungen anbietet, oder ob er die Gebrauchsgegenstaende persoenlich vor Ort abgeben muss. 
* Der Nutzer kann auf einer Map-Screen die Standorte aller Einrichtungen betrachten. 
* Der Nutzer kann die Standorte per Filterfunktion auf einen Bezirk seiner Wahl eingrenzen.

##Wie kann ich Give and Take benutzen?##
* Die beigefuegte APK auf ein Android lauffaehiges Geraet installieren, oder per Android Studio oeffnen.
* Zum Importieren des Projekts in Android Studio geben Sie folgenden Befehl ein:  
```
git clone https://NUTZERNAME@bitbucket.org/tms_ws16_01/giveandtake.git
```
* Es sollte mindestens die SDK 17 installiert sein. Empfehlenswert ist die SDK 24.

##Wie kann ich zum Projekt beitragen?##
* Als Entwickler sind wir offen fuer neue Ideen und Verbesserungen. Per e-Mail koennen Anfragen und weitere Informationen (bereit) gestellt werden.

##Informationen zum Autor##
* Gruppenarbeit im Modul "Technik mobiler Systeme"
* Gruppe 1 : Ali Al-Ali, Burak Ceylan, Elisha-Daniel Santiago