package de.ceylan.burak.giveandtake;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.GoogleMap;

public class StartActivity extends AppCompatActivity {


    private GoogleMap mMap;
    private Context context;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        pagerAdapter.addFragments(new ListFragment(), getString(R.string.list));
        pagerAdapter.addFragments(new MapFragment(), getString(R.string.map));
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }
}
