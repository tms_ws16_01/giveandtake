package de.ceylan.burak.giveandtake;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        XmlParser.Item einrichtung = (XmlParser.Item) getIntent().getSerializableExtra(getString(R.string.einrichtung));
        setSupportActionBar(toolbar);

        Fragment fragment = DetailFragment.newInstance(einrichtung);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.detail_container, fragment);
       // transaction.addToBackStack(null);

        transaction.commit();






    }

}
