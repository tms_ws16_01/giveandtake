package de.ceylan.burak.giveandtake;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    DownloadXml async;
    Spinner filter;
    public static ArrayList<XmlParser.Item> arrayList = new ArrayList<XmlParser.Item>();
    CustomArrayAdapter adapter;
    ListView listView;
    Context context;


    public ListFragment() {
        // Required empty public constructor
    }





    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        async = new DownloadXml();
        List<XmlParser.Item> result = null;
        async.execute(getString(R.string.opendatasource));
        int counter = 0;
        while (counter < 1) {
            if (async.getItemList() != null) {
                result = async.getItemList();
                arrayList.addAll(result);
                counter = 2;
            }
        }
        adapter = new CustomArrayAdapter(getActivity(), arrayList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewgroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, viewgroup, false);
        listView = (ListView) view.findViewById(R.id.list);
        System.out.println(adapter);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                XmlParser.Item e = adapter.getItem(i);
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra(getString(R.string.einrichtung), e);
                startActivity(intent);

            }
        });
        filter = (Spinner) view.findViewById(R.id.spinner);
        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<XmlParser.Item> list = new ArrayList<>();
                ArrayList<XmlParser.Item> listFilter = new ArrayList<>();
                list.addAll(async.getItemList());
                if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.alle))) {
                    listFilter.addAll(list);
                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.spandau))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.spandau))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }
                    System.out.println(list.size());

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.kreuzberg))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.kreuzberg))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.charlottenburg))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.charlottenburg))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.neukoelln))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.neukoelln))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.pankow))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.pankow))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.prenzlauerberg))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.prenzlauerberg))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.wilmersdorf))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.wilmersdorf))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.wedding))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.wedding))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.mitte))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.mitte))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.tempelhof))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.tempelhof))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.treptowkoepenick))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.treptowkoepenick))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }

                } else if (filter.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.zehlendorf))) {

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).bezirk.equalsIgnoreCase(getString(R.string.zehlendorf))) {
                            listFilter.add(list.get(i));
                            System.out.println(listFilter.size());
                        }
                    }
                }
                adapter.setItems(listFilter);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

}
