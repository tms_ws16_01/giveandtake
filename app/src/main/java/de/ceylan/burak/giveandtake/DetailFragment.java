package de.ceylan.burak.giveandtake;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    XmlParser.Item einrichtung;


    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        einrichtung = (XmlParser.Item) getArguments().getSerializable(getString(R.string.einrichtung));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        TextView eName = (TextView) view.findViewById(R.id.detail_titel);
        TextView eStrasse = (TextView) view.findViewById(R.id.detail_strasse);
        TextView ePlz = (TextView) view.findViewById(R.id.detail_plz);
        TextView eBeschreibung = (TextView) view.findViewById(R.id.detail_beschreibung);
        TextView eInfo = (TextView) view.findViewById(R.id.detail_zusatz);

        eName.setText(einrichtung.name);
        eStrasse.setText(einrichtung.adresse);
        ePlz.setText(einrichtung.plz);
        eBeschreibung.setText(einrichtung.gueter);
        eInfo.setText(einrichtung.transport);


        return view;
    }

    public static DetailFragment newInstance(XmlParser.Item e) {
        DetailFragment fragmentDemo = new DetailFragment();
        Bundle args = new Bundle();

        args.putSerializable("einrichtung", e);
        fragmentDemo.setArguments(args);
        return  fragmentDemo;
    }

}
