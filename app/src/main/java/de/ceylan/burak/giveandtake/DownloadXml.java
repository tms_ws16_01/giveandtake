package de.ceylan.burak.giveandtake;

import android.os.AsyncTask;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Beussel on 17.12.16.
 */

public class DownloadXml extends AsyncTask<String, Void, String> {
    private List<XmlParser.Item> items = null;
    public static final int READ_TIMEOUT = 10000;
    public static final int CONNECT_TIMEOUT = 15000;

    @Override
    public String doInBackground(String... urls) {
        try {
            return loadXmlFromNetwork(urls[0]);
        } catch (IOException e) {
            return null;
        } catch (XmlPullParserException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {


    }


    private String loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {

        XmlParser xmlParser = new XmlParser();
        InputStream stream = null;

        try {
            stream = downloadUrl(urlString);
            items = xmlParser.parse(stream);

        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return null;

    }


    private InputStream downloadUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(READ_TIMEOUT);
        conn.setConnectTimeout(CONNECT_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);

        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
    }

    public List<XmlParser.Item> getItemList() {
        return items;
    }
}



