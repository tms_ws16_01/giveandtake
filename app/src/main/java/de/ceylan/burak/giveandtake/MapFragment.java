package de.ceylan.burak.giveandtake;


import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;


/**
 * A simple Fragment subclass.
 */
public class MapFragment extends SupportMapFragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleMapOptions options;
    private static final float HBLUE = 197;
    BitmapDescriptor bitmapDescriptor;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bitmapDescriptor = (BitmapDescriptor) BitmapDescriptorFactory.defaultMarker(HBLUE);

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        final double latB = 52.5097;
        final double lngB = 13.3961;
        LatLng berlin = new LatLng(latB, lngB);

        final double latH = 52.4342;
        final double lngH = 13.47800;
        // Add a marker in Sydney and move the camera
        LatLng home = new LatLng(latH, lngH);

        newMarkers();

        final int ten = 10;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(berlin, ten));




        mMap.getUiSettings().setZoomControlsEnabled(true);




    }

    private void newMarkers() {
        int count = 0;
        while (count < 1) {
            if (ListFragment.arrayList != null) {


                for (int i = 0; i < ListFragment.arrayList.size(); i++) {
                    XmlParser.Item item = ListFragment.arrayList.get(i);

                    String str = item.adresse;
                    String[] arr = str.split(" ");
                    str = arr[0] + " " + arr[1];


                    LatLng neu = geoLatLng(str);
                    mMap.addMarker(new MarkerOptions().position(neu).title(item.name).icon(bitmapDescriptor));
                }
                count = 2;
            }
        }


    }
    public LatLng geoLatLng(String anschrift) {
        Address address = geoAdresse(anschrift);
        return new LatLng(address.getLatitude(), address.getLongitude());
    }

    private Address geoAdresse(String anschrift) {
        Geocoder gc = new Geocoder(getContext());
        List<Address> list = null;
        try {
            list = gc.getFromLocationName(anschrift, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = list.get(0);
        return address;
    }
}
