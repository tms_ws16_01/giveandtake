package de.ceylan.burak.giveandtake;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Beussel on 01.12.16.
 * (Bei vielen Versuchen, die Strings in dieser Klasse NICHT hardgecoded zu implementieren,
 * funktionierte die App nicht mehr, weswegen sie hier leider hardgecoded geblieben sind)
 */

public class XmlParser {

    private static final String NS = null;



    public List<Item> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List<Item> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Item> items = new ArrayList<Item>();

        parser.require(XmlPullParser.START_TAG, NS, "xml");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("index")) {
                parser.require(XmlPullParser.START_TAG, NS, "index");
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String name2 = parser.getName();
                    if (name2.equals("item")) {
                        items.add(readItem(parser));
                    } else {
                        skip(parser);
                    }
                }
            } else {
                skip(parser);
            }
        }
        return items;
    }


    public static final class Item implements Serializable {
        public final String name;
        public final String gueter;
        public final String transport;
        public final String anschrift;
        public final String bezirk;
        public final String plz;
        public final String adresse;

        private Item(String name, String gueter, String transport, String anschrift, String bezirk, String plz, String adresse) {
            this.name = name;
            this.gueter = gueter;
            this.transport = transport;
            this.anschrift = anschrift;
            this.bezirk = bezirk;
            this.plz = plz;
            this.adresse = adresse;
        }
    }

    private Item readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NS, "item");
        String name = null;
        String gueter = null;
        String transport = null;
        String anschrift = null;
        String bezirk = null;
        String plz = null;
        String adresse = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nameTmp = parser.getName();
            if (nameTmp.equals("name")) {
                name = readName(parser);
            } else if (nameTmp.equals("gueter")) {
                gueter = readGueter(parser);
            } else if (nameTmp.equals("transport")) {
                transport = readTransport(parser);
            }  else if (nameTmp.equals("anschrift")) {
                anschrift = readAnschrift(parser);
            } else if (nameTmp.equals("bezirk")) {
                bezirk = readBezirk(parser);
            } else if (nameTmp.equals("plz")) {
                plz = readPlz(parser);
            } else if (nameTmp.equals("adresse")) {
                adresse = readAdresse(parser);
            } else {
                skip(parser);
            }
        }
        return new Item(name, gueter, transport, anschrift, bezirk, plz, adresse);
    }

    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "name");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "name");
        return name;
    }
    private String readPlz(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "plz");
        String plz = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "plz");
        return plz;
    }
    private String readAdresse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "adresse");
        String adresse = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "adresse");
        return adresse;
    }

    private String readGueter(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "gueter");
        String gueter = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "gueter");
        return gueter;
    }

    private String readTransport(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "transport");
        String transport = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "transport");
        return transport;
    }

    private String readAnschrift(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "anschrift");
        String anschrift = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "anschrift");
        return anschrift;
    }

    private String readBezirk(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NS, "bezirk");
        String bezirk = readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, "bezirk");
        return bezirk;
    }


    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
                default:
            }
        }
    }
}





