package de.ceylan.burak.giveandtake;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by burak on 02.12.16.
 */

public class CustomArrayAdapter extends ArrayAdapter<XmlParser.Item> {


    public CustomArrayAdapter(Context context, ArrayList<XmlParser.Item> objects) {
        super(context, 0, objects);
    }

    public void setItems(ArrayList<XmlParser.Item> list) {
        this.clear();
        this.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        XmlParser.Item e = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_cell, parent, false);
        }

        TextView eName = (TextView) convertView.findViewById(R.id.list_cell_title);
        TextView eStrasse = (TextView) convertView.findViewById(R.id.list_cell_address);
        TextView ePlz = (TextView) convertView.findViewById(R.id.detail_plz);


        eName.setText(e.name);
        eStrasse.setText(e.adresse + ", " + e.plz + " " + R.string.berlin);





        return convertView;
    }
}
