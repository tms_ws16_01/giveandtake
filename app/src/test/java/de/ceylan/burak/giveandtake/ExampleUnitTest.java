package de.ceylan.burak.giveandtake;


import org.junit.Test;

import static org.junit.Assert.assertNotSame;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void getItemTest1() {
        PagerAdapter pagerAdapter = new PagerAdapter(null);
        assertSame(new ListFragment(), pagerAdapter.getItem(1));
    }
    @Test
    public void getItemTest2() {
        PagerAdapter pagerAdapter = new PagerAdapter(null);
        assertNotSame(new ListFragment(), pagerAdapter.getItem(0));
    }
    @Test
    public void getItemTest3() {
        PagerAdapter pagerAdapter = new PagerAdapter(null);
        assertNotSame(new ListFragment(), pagerAdapter.getItem(2));
    }
    
}